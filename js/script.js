/**
 * @since 2018 /11/ 21
 * @description headstart (pvt) ltd New scorm standard script
 * @author Kusal Rathna Bandra
 * @access private
 */

// if(window.innerHeight > window.innerWidth){
//   alert("Please use Landscape!");
// }

// full screen page Load
function Fullscreen() {
  var element = document.getElementById("myVideo");
  //requestFullscreen is used to display an element in full screen mode.
  if ("requestFullscreen" in element) {
    element.requestFullscreen();
  } else if ("webkitRequestFullscreen" in element) {
    element.webkitRequestFullscreen();
  } else if ("mozRequestFullScreen" in element) {
    element.mozRequestFullScreen();
  } else if ("msRequestFullscreen" in element) {
    element.msRequestFullscreen();
  }

}
// Exit full screen 
function openFullscreen() {
  var element = document.documentElement;
  //requestFullscreen is used to display an element in full screen mode.
  if ("requestFullscreen" in element) {
    element.requestFullscreen();
  } else if ("webkitRequestFullscreen" in element) { /* Chrome, Safari and Opera */
    element.webkitRequestFullscreen();
  } else if ("mozRequestFullScreen" in element) { /* Firefox */
    element.mozRequestFullScreen();
  } else if ("msRequestFullscreen" in element) { /* IE/Edge */
    element.msRequestFullscreen();
  }
}

function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) { /* Firefox */
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) { /* IE/Edge */
    document.msExitFullscreen();
  }
}

//context menu disable
// document.oncontextmenu = function () {
//   return false;
// }
let pasent = 25;
let a = 1; /* slide Count */
let b = "This is your current slid  "

//set page on loding with jQuery
$(document).ready(function () {
  $('#demo').load("slids/slid" + a + ".html");
  document.getElementById("slid").innerHTML = b + a;
  if (a == 1) {
    document.getElementById("boxbtn2").style.visibility = "hidden";
  } else {
    document.getElementById("boxbtn2").style.visibility = "visible";
  }
  document.getElementById("pross").style.width = pasent + "%"; // progress 
});

function next() {  // Next slide function 
  a++;
  pasent = pasent + 25;
  $('#demo').load("slids/slid" + a + ".html");
  document.getElementById("slid").innerHTML = b + a;
  if (a == 4) { // this is max slide caunt 
    document.getElementById("boxbtn3").style.visibility = "hidden";
  } else {
    document.getElementById("boxbtn2").style.visibility = "visible";
  }
  if (a == 1) {
    document.getElementById("boxbtn2").style.visibility = "hidden";
  } else {
    document.getElementById("boxbtn2").style.visibility = "visible";
  }
  document.getElementById("pross").style.width = pasent + "%";// progress 
  console.log(pasent);

}

function priv() { // previous slide function 
  a--;
  pasent = pasent - 25;
  $('#demo').load("slids/slid" + a + ".html");
  document.getElementById("slid").innerHTML = b + a;
  if (a == 1) {// this is min slide caunt 
    document.getElementById("boxbtn2").style.visibility = "hidden";
  } else {
    document.getElementById("boxbtn2").style.visibility = "visible";
  }
  if (a == 4) {
    document.getElementById("boxbtn3").style.visibility = "hidden";
  } else {
    document.getElementById("boxbtn3").style.visibility = "visible";
  }
  console.log(a);
  document.getElementById("pross").style.width = pasent + "%";// progress 
}

//sidenav bar script  
$(document).ready(function () {
  $('.sidenav').sidenav();

});

//tool tipo
$(document).ready(function () {
  $('.tooltipped').tooltip();
});

// $(document).ready(function () {
//   $("#toolbar").delay(220).fadeOut();
// });

// $("body").click(function(){
//   $("#toolbar") .delay(30).fadeIn();
// });

// $("body").click(function () {
//   $("#toolbar").fadeOut();
// });

// $( "body" ).mousemove(function( event ) {
//   $("#toolbar") .delay(300).fadeIn();
// });

$("#cont").hover(function () {
  $("#toolbar,.p").delay(50).fadeIn();
  $("#main").addClass("container");

  // $("#myVideo").css("width", "70%");
});

$("#demo,#myVideo").hover(function () {
  $("#toolbar").delay(300).fadeOut();


});